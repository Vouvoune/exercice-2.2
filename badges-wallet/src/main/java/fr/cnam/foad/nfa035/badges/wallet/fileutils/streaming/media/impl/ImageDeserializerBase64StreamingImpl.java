package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageDeserializer;
import org.apache.commons.codec.binary.Base64InputStream;

import java.io.*;

/**
 * Implémentation Base64 de désérialiseur d'image, basée sur des flux.
 */
public class ImageDeserializerBase64StreamingImpl extends AbstractStreamingImageDeserializer<AbstractImageFrameMedia> {

    private OutputStream sourceOutputStream;

    /**
     * {@inheritDoc}
     *
     * @return le flux d'écriture
     */

    @Override
    public OutputStream getSourceOutputStream() {
        return sourceOutputStream;
    }

    /**
     * Constructeur élémentaire
     * 
     * @param sourceOutputStream
     */
    public ImageDeserializerBase64StreamingImpl(OutputStream sourceOutputStream) {
        this.sourceOutputStream = sourceOutputStream;
    }

    /**
     * {@inheritDoc}
     *
     * @param media
     * @return le flux de lecture
     * @throws IOException
     */
    @Override
    public InputStream getDeserializingStream(ImageFrameMedia media) throws IOException {
        return new Base64InputStream(media.getEncodedImageInput());
    }

    /**
     * Désérialise une image depuis un media quelconque vers un support quelconque
     *
     * @param media
     * @return
     * @throws IOException
     */

    @Override
    public final void deserialize(ImageFrameMedia media) throws IOException {
        getDeserializingStream(media).transferTo(getSourceOutputStream());

    }

    /**
     * {@inheritDoc}
     * 
     * @param os
     * @param <T>
     */

    @Override
    public <T extends OutputStream> void setSourceOutputStream(T os) {
        this.setSourceOutputStream = os;
    }

}
