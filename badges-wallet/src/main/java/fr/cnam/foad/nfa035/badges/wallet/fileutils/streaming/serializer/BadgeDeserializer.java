package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;

import java.io.IOException;
import java.io.OutputStream;

public interface BadgeDeserializer<M extends ImageFrameMedia> {

    void deserialize(M media) throws IOException;

    <T extends OutputStream> T getSourceOutputStream();

    <T extends OutputStream> void setSourceOutputStream(T os);
}
