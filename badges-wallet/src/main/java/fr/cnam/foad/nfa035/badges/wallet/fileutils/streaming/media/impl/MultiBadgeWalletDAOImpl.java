package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl;

import fr.cnam.foad.nfa035.badges.wallet.dao.BadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;

public class MultiBadgeWalletDAOImpl implements BadgeWalletDAO {

    public MultiBadgeWalletDAOImpl(String string) {
    }

    @Override
    public void deserialize(ResumableImageFrameMedia media) throws IOException {
        BufferedReader br = media.getEncodedImageReader(true);
        String[] data = br.readLine().split(";");
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(data[2]).transferTo(os);
        }
    }

    private Reader getDeserializingStream(String string) {
        return null;
    }

    private OutputStream getSourceOutputStream() {
        return null;
    }

    public void addBadge(File image) {
    }

}
