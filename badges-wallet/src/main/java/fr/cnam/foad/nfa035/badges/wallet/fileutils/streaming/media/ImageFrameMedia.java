package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;

public interface ImageFrameMedia<T> {


    /*
     * {@inheritDoc}
     */

    T getChannel();

    /**
     * Permet d'obtenir le flux d'écriture sous-tendant à notre canal
     *
     * @return le flux d'écriture
     * @throws IOException
     */
    public abstract Writer getEncodedImageOutput() throws IOException;

}
