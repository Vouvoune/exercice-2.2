package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DatabaseDeserializer;
import org.apache.commons.codec.binary.Base64InputStream;

import java.io.*;

public class ImageDeserializerBase64DatabaseImpl implements DatabaseDeserializer<ResumableImageFrameMedia> {



    @Override
    public void deserialize(ResumableImageFrameMedia media) throws IOException {

    }

    private ResumableImageFrameMedia<Object> media;
    // 1. Récupération de l'instance de lecture séquentielle du fichier de base csv
    BufferedReader br;

    {
        try {
            br = media.getEncodedImageReader(true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // 2. Lecture de la ligne et parsage des différents champs contenus dans la
    // ligne
    String[] data;

    {
        try {
            data = br.readLine().split(";");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // 3. Désérialisation de l'image Base64 + écriture en clair dans le flux de
    // restitution au format source

    {
        try {
            OutputStream os = getSourceOutputStream();
            try {
                getDeserializingStream(data[2]).transferTo(os);
            } catch (IOException e) {
                e.printStackTrace();
            }

        } finally {

        }
    }

    @Override
    public InputStream getDeserializingStream(String data) throws IOException {
        return new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
    }

    @Override
    public <T extends OutputStream> T getSourceOutputStream() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T extends OutputStream> void setSourceOutputStream(T os) {
        // TODO Auto-generated method stub

    }
}
