package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Classe abstraite visant à structurer/guider le développement dee manière
 * rigoureuse
 * Elle s'applique à tout objet représentant un média de sérialisation.
 * Les implémentations sous-jacentes peuvent revêtir des aspects de fichiers ou
 * bien de flux.
 *
 * @param <T>
 */
public abstract class AbstractImageFrameMedia<T> implements ImageFrameMedia<T> {

    /**
     * le canal de distribution de notre image sérialisée, potentiellement un
     * Fichier ou bien même un Flux
     */
    private T channel;

    /**
     * Permet d'obtenir le canal de distribution de notre image sérialisée,
     * potentiellement un Fichier ou bien même un Flux
     *
     * @return le canal
     */
    public T getChannel() {
        return channel;
    }

    /**
     * Permet de définir le canal de distribution de notre image sérialisée,
     * potentiellement un Fichier ou bien même un Flux
     *
     * @param channel
     */
    public void setChannel(T channel) {
        this.channel = channel;
    }

    /**
     * Constructeur simple
     * 
     * @param channel
     */
    public AbstractImageFrameMedia(T channel) {
        this.channel = channel;
    }

    /**
     * Permet d'obtenir le flux de lecture sous-tendant à notre canal
     *
     * @return le flux de lecture
     * @throws IOException
     */
    public abstract InputStream getEncodedImageInput() throws IOException;
}
